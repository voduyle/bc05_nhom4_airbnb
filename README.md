### For Mentor

-   Link deploy https://airbnb-sigma-three.vercel.app/
-   Link youtube https://youtu.be/Le4FrGMpGMU
-   Task and wireframe
    https://drive.google.com/drive/folders/1cuRUQnf97DffhqAPB5FvLiGvZ8aDE4gR?usp=sharing
# Movie project

Welcome to the AirBnb website. This is a project programmed by two web developers, Tam and Phong.
Thank for watching !

<div align="center">
  <a href="https://airbnb-sigma-three.vercel.app/">
    <img src="./src/Assets/airbnb-logo.png" alt="Logo" width="100" height="70">
  </a>
  <p align="center">
    <a href="https://airbnb-sigma-three.vercel.app/">View Demo</a>
  </p>
</div>

<br />

<div align="center">
  <img src="./src/Assets/HomePage.jpeg" alt="Screen" width="100%" height="100%">
  <a href="./src/Assets/HomePage.jpeg">HomePage</a>
  <a href="./src/Assets/DetailPage.jpeg">DetailPage</a>
  <a href="./src/Assets/RoomPage.jpeg">RoomPage</a>
  <a href="./src/Assets/UserPage.jpeg">PersonalPage</a>
  <a href="./src/Assets/AdminPage.jpeg">AdminPage</a>
</div>

## Install

In the project directory, you can run:

```sh
npm i
npm start
```

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Requests

-   Create an account or you can use
    adminAccount :
    vanphong1993@gmail.com/123456

-   You can't access the site AdminPage without an admin account.
-   After logging in, a link to Admin Page will appear in the DropMenu on the right corner of the screen.
-   You can't book room if you are not logged in.

### Third Party libraries used except for React and Tailwind

-   [react-rounter-dom](https://reactrouter.com/en/main)
-   [axios](https://github.com/axios/axios)
-   [antd](https://ant.design/)
-   [react-redux](https://react-redux.js.org/)
-   [redux-thunk](https://github.com/reduxjs/redux-thunk)

### Todo

-   Use database from to CyberSoft center.
-   UserPage is written by me and Tam wrote AdminPage.
-   Use Redux Thunk as a middleware for data processing then render on the website with ReacHook.
-   Use Tailwind as a primary tool for design UI/UX.

## Description

### AminPage

-   Find user,booked room information, register and delete.
-   Find room,position information, create, add picture and detele.

### UserPage

-   Find room information, quick view.
-   Book room.
-   View history and edit user information.
